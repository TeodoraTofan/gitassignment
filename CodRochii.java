package ro.ase.csie.licenta;

public class CodRochii {
	public int codRochieScurta;
	public int codRochieSeara;
	public int codRochieMidi;
	public int codRochieOcazieSpeciala;
	private static float PRET_MINIM=200;
	public float pretRochie;
	
	public CodRochii(int codRochieScurta, int codRochieSeara, int codRochieMidi, int codRochieOcazieSpeciala,
			float pretRochie) {
		super();
		this.codRochieScurta = codRochieScurta;
		this.codRochieSeara = codRochieSeara;
		this.codRochieMidi = codRochieMidi;
		this.codRochieOcazieSpeciala = codRochieOcazieSpeciala;
		this.pretRochie = pretRochie;
	}
	
	

}
